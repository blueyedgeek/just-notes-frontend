import React, { Component } from 'react';

import './Footer.css';

export default class Footer extends Component {
  render() {
    return (
      <footer>
        <p>
          Made with<i className="fas fa-heart"></i>
          and<i className="fas fa-coffee"></i>
          by
          <a href="http://github.com/blueyedgeek"> blueyedgeek</a>
        </p>
      </footer>
    );
  }
}
