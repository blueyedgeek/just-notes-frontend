import React from 'react';
import PropTypes from 'prop-types';

import './FormErrors.css';

const FormErrors = ({ errorMessages, title }) => (
  <div className="form-errors">
    <h3>
      {title}
    </h3>
    <ul>
      { errorMessages.map(msg => (
        <li
          key={msg}
        >
          <i className="fas fa-times"></i>
          {msg}
        </li>
      )) }
    </ul>
  </div>
);

FormErrors.propTypes = {
  errorMessages: PropTypes.arrayOf(PropTypes.string).isRequired,
  title: PropTypes.string.isRequired
};

export default FormErrors;
