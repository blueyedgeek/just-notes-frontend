import React from 'react';
import { Link } from 'react-router-dom';
import { authContainer } from '../../../api/helperUtils';

import './Navbar.css';

const navLinks = () => {
  if (authContainer.isAuthenticated()) {
    return (
      <Link
        to="/"
        key="logout"
        onClick={authContainer.removeAuthToken}
      >
        Logout
      </Link>
    );
  } else {
    return [
      <Link to="/signup" key="signup">Sign up</Link>,
      <Link to="/login" key="login" className="highlight">Login</Link>
    ];
  }
}

export default () => (
  <nav className="navigation">
    <div className="logo">
      <Link to="/">
        <i className="far fa-sticky-note"></i>
        Just Notes
      </Link>
    </div>

    <div className="auth-links">
      {navLinks()}
    </div>
  </nav>
);
