import React from 'react';
import { shallow } from 'enzyme';
import Signup from '../Signup';

jest.mock('../../../api/registerUser');
jest.mock('react-router-dom/Redirect');

describe('<Signup />', () => {

  describe('signing up with invalid credentials', () => {
    test('user signup is prevented', async () => {
      const container = shallow(<Signup />);
      const emailInput = container.find('[name="email"]');
      const passwordInput = container.find('[name="password"]');

      emailInput.simulate('change',
        { target: { value: 'fake', name: 'email' } }
      );
      passwordInput.simulate('change',
        { target: { value: 'fake', name: 'password' } }
      );
      await container.find('form').props().onSubmit({preventDefault: () => {}});
      container.update();

      expect(container.state('errors')).toEqual([
        'Email is invalid',
        'Password is too short (minimum is 8 characters)'
      ]);
    });
  });

  describe('signing up with valid credentials', () => {
    test('user signup is allowed', async () => {
      const container = shallow(<Signup />);
      expect(container.state('validSignup')).toBe(false);

      const emailInput = container.find('[name="email"]');
      const passwordInput = container.find('[name="password"]');

      emailInput.simulate('change',
        { target: { value: 'test@example.com', name: 'email' } }
      );
      passwordInput.simulate('change',
        { target: { value: 'str0ngp@ss!', name: 'password' } }
      );
      await container.find('form').props().onSubmit({preventDefault: () => {}});
      container.update();

      expect(container.state('validSignup')).toBe(true);
    });
  });
});
