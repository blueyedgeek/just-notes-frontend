import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import { registerUser } from '../../api/registerUser';
import { authContainer } from '../../api/helperUtils';
import FormErrors from '../shared/FormErrors/FormErrors';

import './Signup.css';

export default class Signup extends Component {
  state = {
    email: '',
    password: '',
    errors: [],
    validSignup: false,
    requestActive: false
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleSubmit = async (e) => {
    e.preventDefault();
    this.setState({ requestActive: true });
    const email = this.state.email;
    const password = this.state.password;
    const result = await registerUser(email, password);

    if (result.errors) {
      this.setState({ errors: result.errors, requestActive: false });
    } else {
      this.setState({ validSignup: true, requestActive: false });
    }
  }

  render() {
    const { errors, validSignup, email, password, requestActive } = this.state;

    return (
      <section className="signup main-content">
        <h1>Create an Account</h1>

        <form onSubmit={this.handleSubmit}>
          { errors.length > 0 &&
            <FormErrors
              title="The following errors prevented user signup:"
              errorMessages={errors}
            />
          }

          <div className="form-group">
            <label>
              Email:
            </label>

            <div className="input-wrapper">
              <i className="fas fa-envelope"></i>
              <input
                type="email"
                placeholder="john@example.com"
                name="email"
                value={email}
                onChange={this.handleChange}
                required
              />
            </div>
          </div>

          <div className="form-group">
            <label>
              Password:
            </label>
            <div className="input-wrapper">
              <i className="fas fa-key"></i>
              <input
                type="password"
                placeholder="********"
                name="password"
                value={password}
                minLength="8"
                onChange={this.handleChange}
                required
              />
            </div>
          </div>

          <div className="form-group">
            <button type="submit" className="button">
              { !requestActive && 'Submit' }

              { requestActive && (
                <span>
                  Submitting
                  <i className="fas fa-atom fa-spin"></i>
                </span>
              )}
            </button>
          </div>

          <p>
            Already have an account?
            <Link to="/login">Login</Link>
          </p>
        </form>

        { validSignup && <Redirect to="/login" /> }
        { authContainer.isAuthenticated() && <Redirect to="/dashboard" /> }
      </section>
    );
  }
}
