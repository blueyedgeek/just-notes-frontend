import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { authenticateUser } from '../../api/authenticateUser';
import { authContainer } from '../../api/helperUtils';
import FormErrors from '../shared/FormErrors/FormErrors';

import './Login.css';

export default class Login extends Component {
  state = {
    email: '',
    password: '',
    errors: [],
    requestActive: false,
    validLogin: false
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleSubmit = async (e) => {
    e.preventDefault();
    this.setState({ requestActive: true });
    const email = this.state.email;
    const password = this.state.password;
    const result = await authenticateUser(email, password);

    if (result.errors) {
      this.setState({
        errors: ["Invalid email or password"],
        requestActive: false
      });
    } else {
      authContainer.setAuthToken(result.access_token);
      this.setState({ validLogin: true, requestActive: false });
    }
  }

  render() {
    const { email, password, requestActive, errors, validLogin } = this.state;

    return (
      <section className="login main-content">
        <h1>Login to your Account</h1>

        <form onSubmit={this.handleSubmit}>
          { errors.length > 0 &&
            <FormErrors
              title="We couldn't sign you in."
              errorMessages={errors}
            />
          }

          <div className="form-group">
            <label>
              Email:
            </label>

            <div className="input-wrapper">
              <i className="fas fa-envelope"></i>
              <input
                type="email"
                placeholder="john@example.com"
                name="email"
                value={email}
                onChange={this.handleChange}
                required
              />
            </div>
          </div>

          <div className="form-group">
            <label>
              Password:
            </label>
            <div className="input-wrapper">
              <i className="fas fa-key"></i>
              <input
                type="password"
                placeholder="********"
                name="password"
                value={password}
                onChange={this.handleChange}
                required
              />
            </div>
          </div>

          <div className="form-group">
            <button type="submit" className="button">
              {!requestActive && 'Submit'}

              {requestActive && (
                <span>
                  Submitting
                  <i className="fas fa-atom fa-spin"></i>
                </span>
              )}
            </button>
          </div>

          <p>
            Don't have an account?
            <Link to="/signup">Signup</Link>
          </p>
        </form>

        { (authContainer.isAuthenticated() || validLogin) &&
          <Redirect to="/dashboard" /> }
      </section>
    )
  }
}
