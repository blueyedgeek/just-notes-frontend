import React from 'react';
import renderer from 'react-test-renderer';
import { StaticRouter } from 'react-router';
import Home from '../Home';

describe('<Home />', () => {
  it('renders without any errors', () => {
    const component = renderer.create(
      <StaticRouter context={{}}>
        <Home />
      </StaticRouter>
    );
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });
});
