import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import { authContainer } from '../../api/helperUtils';

import './Home.css';

const Home = () => (
  <section className="home main-content">
    <section className="welcome">
      <div className="welcome-message">
        <h1>
          Easily save your notes wherever you are.
        </h1>
        <p>
          Just notes allows you to document your ideas without all the
          effort.
        </p>

        <Link to="/signup" className="button">Get Started</Link>
      </div>
    </section>

    <section className="features">
      <div className="feature">
        <i className="fas fa-cloud-upload-alt"></i>
        <h2>It's Free</h2>
        <p>
          Everything is completely free! No hidden charges, no hidden costs,
          it's all free.
        </p>
      </div>

      <div className="feature">
        <i className="fas fa-folder"></i>
        <h2>Stay Organized</h2>
        <p>
          Tag your notes and find them easily using instant search.
        </p>
      </div>

      <div className="feature">
        <i className="fas fa-share"></i>
        <h2>Share your thoughts</h2>
        <p>
          Collaborate with other users, document ideas and share with other
          people.
        </p>
      </div>
    </section>

    { authContainer.isAuthenticated() && <Redirect to="/dashboard" /> }
  </section>
);

export default Home;
