import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { authContainer } from '../../api/helperUtils';
import { fetchAllNotes, createNote, updateNote, deleteNote } from '../../api/noteApi';

import './Dashboard.css';

const limitText = (text, length) => {
  const suffix = text.length > length ? '...' : '';

  return `${text.slice(0, length)}${suffix}`;
};

const debounce = (fn, wait) => {
  let timeout;

  return function () {
    let context = this;
    let args = arguments;
    let later = function () {
      timeout = null;
      fn.apply(context, args);
    };

    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
  };
};

export default class Dashboard extends Component {
  state = {
    activeNote: null,
    sidebarViewState: '',
    activeNoteState: 'hide-sm',
    notes: [],
    title: '',
    content: '',
  };

  showNote = (e) => {
    const noteId = parseInt(e.currentTarget.dataset.activeNote);
    const activeNote = this.state.notes.find(note => note.id === noteId);

    this.setState({
      activeNote,
      sidebarViewState: 'hide-sm',
      activeNoteState: '',
      title: activeNote.title,
      content: activeNote.content
    });
  }

  hideNote = () => {
    const notes = this.state.notes;

    this.setState({
      activeNote: notes[0],
      sidebarViewState: '',
      activeNoteState: 'hide-sm'
    });
  }

  createNewNote = async (e) => {
    e.preventDefault();
    const title = 'Title';
    const content = 'Click to edit';
    const result = await createNote(title, content);

    if (!result.errors) {
      const allNotes = this.state.notes;
      allNotes.unshift(result);
      const activeNote = allNotes[0];

      this.setState({
        notes: allNotes,
        activeNote,
        title: activeNote.title,
        content: activeNote.content
      });
    }
  }

  updateNote = debounce(async (note, attr, value) => {
    const title = attr === 'title' ? value : note.title;
    const content = attr === 'content' ? value : note.content;
    const result = await updateNote(note.id, title, content);

    if (!result.errors) {
      const allNotes = this.state.notes;
      const activeNote = allNotes.find(note => note.id === result.id);

      activeNote.title = result.title;
      activeNote.content = result.content;

      this.setState({
        notes: allNotes,
        activeNote
      });
    }
  }, 1500)

  deleteNote = () => {
    let allNotes = this.state.notes;
    const activeNote = this.state.activeNote;
    const shouldDeleteNote = window.confirm('Are you sure about this?');

    if (shouldDeleteNote) {
      deleteNote(activeNote.id);
      allNotes = allNotes.filter(note => note.id !== activeNote.id);
      const newActiveNote = allNotes[0];

      this.setState({
        notes: allNotes,
        activeNote: newActiveNote,
        title: newActiveNote.title,
        content: newActiveNote.content
      });
    }
  }

  handleChange = (e) => {
    e.persist();
    const activeNote = this.state.activeNote;
    const attr = e.target.dataset.attr;
    const value = e.target.value;

    this.setState({ [attr]: value });

    this.updateNote(activeNote, attr, value);
  }

  setDefaultFocus = (e) => {
    if (!e.target.dataset.attr) this.content.focus();
  }

  async componentDidMount() {
    const result = await fetchAllNotes();

    if (!result.errors && result.length) {
      const activeNote = result[0];

      this.setState({
        notes: result,
        activeNote,
        title: activeNote.title,
        content: activeNote.content
      });
    }
  }

  render() {
    const { notes,
      sidebarViewState,
      activeNoteState,
      title,
      content } = this.state;

    return (
      <section className="main-content dashboard">
        {!notes.length &&
          <p className="no-notes">
            <i className="fas fa-sticky-note fa-5x"></i>
            No notes created yet.
            <a href="/dashboard" onClick={this.createNewNote}> Create a note</a>
          </p>
        }
        {!!notes.length && [
          <aside className={`sidebar ${sidebarViewState}`} key="aside">
            <a
              href="/dashboard"
              onClick={this.createNewNote}
              className="highlight"
            >
              New Note
              </a>
            {notes.map(note => (
              <div
                className="note-summary"
                key={note.id}
                data-active-note={note.id}
                onClick={this.showNote}
              >
                <h3 className="title">
                  {note.title}
                </h3>
                <p className="sub-content">
                  {limitText(note.content, 120)}
                </p>
              </div>
            ))}
          </aside>,

          <main
            className="notes"
            key="main"
            onClick={this.setDefaultFocus}
          >
            <div
              className={`note ${activeNoteState}`}
            >
              <span
                className="delete-note"
                data-attr="delete-note"
                onClick={this.deleteNote}>
                <i className="fas fa-trash-alt"></i>
              </span>

              <span onClick={this.hideNote} className="hide-md close-note">
                <i className="fas fa-times"></i>
              </span>

              <input
                type="text"
                data-attr="title"
                value={title}
                onChange={this.handleChange}
              />

              <textarea
                data-attr="content"
                ref={content => this.content = content}
                value={content}
                onChange={this.handleChange}
              >
              </textarea>
            </div>
          </main>
        ]}

        {!authContainer.isAuthenticated() && <Redirect to="/login" />}
      </section>
    )
  }
}
