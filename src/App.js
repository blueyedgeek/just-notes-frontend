import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Home from './components/Home/Home';
import Signup from './components/Signup/Signup';
import Login from './components/Login/Login';
import Dashboard from './components/Dashboard/Dashboard';
import NavBar from './components/shared/Navbar/Navbar';
import Footer from './components/shared/Footer/Footer';

const App = () => (
  <Router>
    <main className="wrapper">
      <NavBar />

      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/signup" component={Signup} />
        <Route path="/login" component={Login} />
        <Route path="/dashboard" component={Dashboard} />
      </Switch>

      <Footer />
    </main>
  </Router>
);

export default App;
