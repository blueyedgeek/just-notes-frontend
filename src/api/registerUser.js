import { refetch } from './helperUtils';

export const registerUser = (email, password) => {
  return refetch({
    url: '/users',
    method: 'POST',
    body: {
      email, password, password_confirmation: password
    }
  });
};
