import { refetch } from './helperUtils';

export const authenticateUser = (email, password) => {
  return refetch({
    url: '/auth/login',
    method: 'POST',
    body: {
      email, password
    }
  });
};
