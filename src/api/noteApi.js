import { refetch } from './helperUtils';

export const fetchAllNotes = () => {
  return refetch({
    url: '/api/v1/notes',
    method: 'GET',
    isAuthorizedRequest: true
  });
};

export const createNote = (title, content) => {
  return refetch({
    url: '/api/v1/notes',
    method: 'POST',
    body: {
      title, content
    },
    isAuthorizedRequest: true
  });
};

export const updateNote = (id, title, content) => {
  return refetch({
    url: `/api/v1/notes/${id}`,
    method: 'PATCH',
    body: {
      title, content
    },
    isAuthorizedRequest: true
  });
};

export const deleteNote = (id) => {
  return refetch({
    url: `/api/v1/notes/${id}`,
    method: 'DELETE',
    isAuthorizedRequest: true
  });
};
