const API_HOST = process.env.REACT_APP_API_HOST;

const requestOptions = (config) => {
  const options = {
    mode: 'cors',
    method: config.method,
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(config.body)
  };

  if (config.isAuthorizedRequest) {
    Object.assign(options.headers,
      { 'Authorization': `Bearer ${authContainer.getAuthToken()}` }
    );
  }

  return options;
}

const jsonResponse = response => response.json();

const queryResult = (response) => {
  if (response.status >= 200 && response.status < 300) {
    return Promise.resolve(jsonResponse(response));
  } else {
    return Promise.reject(jsonResponse(response));
  }
};

const wrapErrorObject = async (obj) => {
  const result = await obj;
  if (!result.errors) return Object.assign({}, result, { errors: true });

  return result;
};

export const refetch = async (config) => {
  let response = await fetch(`${API_HOST}${config.url}`,
    requestOptions(config)
  );

  try {
    return await queryResult(response);
  } catch (err) {
    return await wrapErrorObject(err);
  }
};

export const authContainer = {
  setAuthToken: token => localStorage.setItem('accessToken', token),
  getAuthToken: () => localStorage.getItem('accessToken'),
  removeAuthToken: () => localStorage.removeItem('accessToken'),
  isAuthenticated: () => !!localStorage.getItem('accessToken')
};
