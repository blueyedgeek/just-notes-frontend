export const registerUser = async (email, password) => {
  if (email === 'fake' && password.length < 8) {
    return {
      errors: [
        'Email is invalid',
        'Password is too short (minimum is 8 characters)'
      ]
    };
  }

  return {};
};
